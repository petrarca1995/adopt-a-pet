package com.adoptapet.service;

import com.adoptapet.domain.AnimalsByZipCode.AnimalsByZipCodeResponse;
import com.adoptapet.domain.BearerTokenUrlResponse;
import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PetDataFetcherServiceImpl implements PetDataFetcherService{

    private final static String bearerTokenRequestUrl ="https://api.petfinder.com/v2/oauth2/token";
    private final static String key = "01LmHIAm9se166EV73A8u0K2ey6RUICMT9zKqoHH02X8pXzJgZ";
    private final static String secret = "WjrIwWJUvVIUGXCHhDwt24ry8doSoi8IuErkuJmL";
    private final static String baseUrl = "https://api.petfinder.com/v2/animals";


    private BearerTokenUrlResponse getBearerToken(String key, String secret) throws IOException{

        CloseableHttpClient httpClient = HttpClients.createDefault();
        String response = null;
        try {

            HttpPost httpPost = new HttpPost(bearerTokenRequestUrl);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("grant_type", "client_credentials"));
            params.add(new BasicNameValuePair("client_id", key));
            params.add(new BasicNameValuePair("client_secret", secret));

//        try {
            //*Define what I am expecting back via request headers (json and UTF-8)
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Accept-Charset", "UTF-8");
            //*Define I am sending form url encoded body and it was decoded using UTF-8
            httpPost.setHeader(HTTP.CONTENT_TYPE,"application/x-www-form-urlencoded;charset=UTF-8");
            //*What charset I am actually using to decode the URL string to bytes
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//            }
//        catch(IOException e){
//            log.error("error", e);
//        }

//        try {

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity != null) {
                //! this method is used to decode the sequence of bytes received in the httpEntity object (within the httpResponse) from the API,
                //! which is the json response represented as a sequence of bytes, into characters, which make up strings, using UTF-8 for decoding (the type of encoding scheme), response specifies the encoding scheme that should be used in the header
                response = EntityUtils.toString(httpEntity);
            }

//        }
//        catch (IOException e){
//            log.error("IO Exception: ",e);
//        }
        }finally {
            httpClient.close();
        }

        Gson gson = new Gson();
        return gson.fromJson(response, BearerTokenUrlResponse.class);
//        return response;
    }

    @Override
    public AnimalsByZipCodeResponse getAnimalsByZipCode(String zipCode) throws IOException{
        HttpGet httpGetRequest = new HttpGet(baseUrl+"?"+"location="+zipCode);


            CloseableHttpClient httpClient = HttpClients.createDefault();


            String jsonStringResponse = null;


        try{
            BearerTokenUrlResponse bearerTokenUrlResponse = getBearerToken(key, secret);

//        try{
            //! Bearer token passed as header

            httpGetRequest.addHeader("Accept-Charset","UTF-8");
            httpGetRequest.addHeader("Accept","application/json");
            httpGetRequest.addHeader("Authorization", "Bearer " + bearerTokenUrlResponse.getAccess_token());
            HttpResponse httpResponse = httpClient.execute(httpGetRequest);

                HttpEntity httpEntity = httpResponse.getEntity();

                if (httpEntity != null) {
                    jsonStringResponse = EntityUtils.toString(httpEntity);
//            }
                }

//        catch (IOException e){
//            log.error("IO exception: ",e);
//        }
        }
            finally {
                httpClient.close();
            }

        //* google library that does the mapping from json string to POJO
        Gson gson = new Gson();
        //!if exception is thrown in try, jsonStringResponse will be null and this line will throw an exception. Should we declare this method throws IO instead and pass resposibility of handling to method caller?
        return gson.fromJson(jsonStringResponse, AnimalsByZipCodeResponse.class);
    }


}
