package com.adoptapet.service;

import com.adoptapet.domain.AnimalsByZipCode.AnimalsByZipCodeResponse;

import java.io.IOException;

public interface PetDataFetcherService {

    public AnimalsByZipCodeResponse getAnimalsByZipCode(String zipCode) throws IOException;




}
