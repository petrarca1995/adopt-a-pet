
package com.adoptapet.domain.AnimalsByZipCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Environment {

    @SerializedName("children")
    @Expose
    private Object children;
    @SerializedName("dogs")
    @Expose
    private Object dogs;
    @SerializedName("cats")
    @Expose
    private Boolean cats;

    public Object getChildren() {
        return children;
    }

    public void setChildren(Object children) {
        this.children = children;
    }

    public Object getDogs() {
        return dogs;
    }

    public void setDogs(Object dogs) {
        this.dogs = dogs;
    }

    public Boolean getCats() {
        return cats;
    }

    public void setCats(Boolean cats) {
        this.cats = cats;
    }

}
