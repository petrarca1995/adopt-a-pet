package com.adoptapet.domain;

import lombok.Data;
import org.apache.http.HttpResponse;
@Data
public class BearerTokenUrlResponse {
    private String token_type;
    private Integer expires_in;
    private String access_token;


}
