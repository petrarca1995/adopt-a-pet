package com.adoptapet;

import com.adoptapet.domain.AnimalsByZipCode.AnimalsByZipCodeResponse;
import com.adoptapet.service.PetDataFetcherServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

@Slf4j
@SpringBootApplication
public class AdoptapetApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(AdoptapetApplication.class, args);
        Object petDataFetcherServiceObject = applicationContext.getBean("petDataFetcherServiceImpl");
        PetDataFetcherServiceImpl petDataFetcherService = (PetDataFetcherServiceImpl) petDataFetcherServiceObject;

//         BearerTokenUrlResponse bearerToken = petDataFetcherService.getBearerToken(petDataFetcherService.getKey(), petDataFetcherService.getSecret());
////       String bearerToken = petDataFetcherService.getBearerToken(petDataFetcherService.getKey(), petDataFetcherService.getSecret());
        AnimalsByZipCodeResponse animalsByZipCodeResponse = null;
        try {
            animalsByZipCodeResponse = petDataFetcherService.getAnimalsByZipCode("43074");
            System.out.println("hi");
        }
        catch(IOException e){
            log.error("IO exception: ", e);
        }
////        System.out.println(bearerToken);
        String[] allBeanNames = applicationContext.getBeanDefinitionNames();
        for (String beanName : allBeanNames) {
            System.out.println(beanName);
        }

    }
}
